###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01d - Crazy Cat Guy - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build the Crazy Cat Guy C program
###
### @author  Brayden Suzuki <braydens@hawaii.edu>
### @date    15 Jan 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = crazyCatGuy

all: $(TARGET)

crazyCatGuy: crazyCatGuy.c
	$(CC) $(CFLAGS) -o $(TARGET) crazyCatGuy.c

test: crazyCatGuy
	./crazyCatGuy

clean:
	rm -f $(TARGET) *.o

