///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - Crazy Cat Guy - EE 205 - Spr 2022
///
 // @file    crazyCatGuy.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o crazyCatGuy crazyCatGuy.c
///
/// Usage:  crazyCatGuy n
///   n:  Sum the digits from 1 to n
///
/// Result:
///   The sum of the digits from 1 to n is XX
///
/// Example:
///   $ ./crazyCatGuy 6
///   The sum of the digits from 1 to 6 is 21
///
/// @author  Brayden Suzuki <braydens@hawaii.edu>
/// @date    17_01_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main( int argc, char* argv[] ) {
   int n = atoi( argv[1] );

   int i, sum = 0;

   for (i = 1; i <= n; ++i)
   {
      sum = sum + i;
   }

   printf("Collars: %d", sum);

   return 0;
}
